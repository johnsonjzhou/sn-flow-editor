'use strict';
const webpack = require('webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

// @see https://github.com/jantimon/html-webpack-plugin
const HtmlWebpackPlugin = require('html-webpack-plugin');

// remove these paths at the start of each build
const pathsToClean = [
  '.*',
  '*.js',
  '*.html'
];

// these files are copied directly from src using copy-webpack-plugin
// mainly used for pwa support
const copyFiles = [
  {
    from: 'src/apache/.htaccess', 
    to: '.htaccess',
    toType: 'file'
  }, 
  {
    from: 'src/html/403.html', 
    to: '403.html'
  }, 
  {
    from: 'src/html/404.html', 
    to: '404.html'
  }, 
  {
    from: 'src/html/500.html', 
    to: '500.html'
  }
];

const npmConfig = require('./package.json');

module.exports = {
  entry: {
    'editor': [ './src/_build.jsx' ]
  },
  resolve: {
    alias: {},
    extensions: ['.js', '.jsx']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules\/(?!flow\-).*/,
        use: "babel-loader"
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules\/(?!flow\-).*/,
        use: "babel-loader"
      },
    ]
  }, 
  plugins: [
    // delete existing packed assets
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: pathsToClean
    }),

    // we are dynamically generating the package file
    // [name].[contenthash].js
    // we are using this plugin to generate a list of <script> tags
    // then this is included within the php render process
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './src/html/index.html'
    }),
    
    // static copy of files
    new CopyPlugin({
      patterns: copyFiles
    }), 

    new webpack.DefinePlugin({
      ___WEBPACK_VERSION___: JSON.stringify(npmConfig.version)
    })
  ]
}