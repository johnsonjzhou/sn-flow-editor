/**
 * Extensions involving EditorView.updateListener
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  onChangeCallback
 */
import { EditorView } from "@codemirror/view";

/**
 * Provides a code mirror extension that invokes a callback when 
 * the viewer document has changed and applying the ViewUpdate object
 * 
 * @see  https://discuss.codemirror.net/t/codemirror-6-proper-way-to-listen-for-changes/2395/11
 * @see  https://codemirror.net/6/docs/ref/#view.ViewUpdate
 * 
 * @var  {function}  callback
 * 
 * @returns  {array}
 */
const onChangeCallback = (callback) => {
  return [
    EditorView.updateListener.of((viewUpdate) => {
      const { docChanged } = viewUpdate;
      docChanged && callback && callback(viewUpdate);
    })
  ];
};

export {
  onChangeCallback
}