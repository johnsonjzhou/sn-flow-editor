/**
 * Markdown Flow Editor for Standard Notes
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  Editor
 */
import React, { memo, useEffect, useRef } from 'react';
import { EditorView } from '@codemirror/view';
import { EditorState } from '@codemirror/state';

import { GlobalStyles } from './styles/global_styles';
import { EditorCMStyles } from './styles/editor_cm_styles';
import { EditorSection } from './styles/editor_main';
import { flowEditorExtensions } from './config/flow_editor_extensions';

const Editor = (props) => {

  const editorContainer = useRef();
  const editor = useRef();

  useEffect(() => {
    editor.current = new EditorView({
      state: EditorState.create({
        //todo  initiate with SN content stream
        doc: "# Title \nThe quick brown fox jumps over the lazy dog.", 
        extensions: flowEditorExtensions
      }),
      parent: editorContainer.current, 
    })
  });

  return (
    <>
      <GlobalStyles/>
      <EditorCMStyles/>
      <EditorSection ref={ editorContainer } />
    </>
  )
}

export default memo(Editor);