/**
 * Styles for the editor's main section
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  EditorSection
 */

import styled from "styled-components";

const EditorSection = styled.section`
  display: block;
  width: 100%;
  height: 100%;
`;

export {
  EditorSection
};