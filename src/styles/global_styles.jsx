/**
 * Global App Styles
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  GlobalStyles
*/

import { createGlobalStyle } from 'styled-components';

const GlobalStyles = createGlobalStyle`

  :root {

  }

  html {
    font-size: var(--sn-stylekit-base-font-size, 12px);
  }

  body {
    position: relative;
    overflow: hidden;
    margin: 0;
    box-sizing: border-box;
  }

  main {
    display: block;
    position: fixed;
    overflow: hidden;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }
`;

export {
  GlobalStyles
}