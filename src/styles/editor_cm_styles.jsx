/**
 * CodeMirror style overrides
 * @author Johnson Zhou <johnson@simplyuseful.io>
 * 
 * We are using styled-components rather than EditorView.theme().
 * For now, it provides more flexibility in defining CSS styles 
 * that can also relate to the CSS variables provided upstream in Standard Notes.
 * 
 * @export  EditorCMStyles
*/

import { createGlobalStyle, css, keyframes } from 'styled-components';

const activeLineBackground = css`
  background-color: var(--cm-flow-activeLine-background);
`;

const cursorBlink = keyframes`
  0% { opacity: 1; }
  10% { opacity: 0; }
  50% { opacity: 0; }
  60% { opacity: 1; }
  100% { opacity: 1; }
`;

const EditorCMStyles = createGlobalStyle`

  :root {
    // supports both light and dark color schemes 
    color-scheme: light dark; 

    // :::::: font ::::::
    --cm-flow-global-font-size: var(--sn-stylekit-base-font-size, 14px);
    --cm-flow-global-letter-spacing: 0.1ch;
    --cm-flow-global-line-spacing: 0.2em;
    --cm-flow-serif-font: 
      var(--sn-stylekit-serif-font, serif);
    --cm-flow-sans-serif-font: 
      var(--sn-stylekit-sans-serif-font, sans-serif);
    --cm-flow-monospace-font: 
      var(--sn-stylekit-monospace-font, 'JetBrains Mono', monospace);
    
    // :::::: colors ::::::
    --cm-flow-black: #000000;
    --cm-flow-grey-1: #191918;
    --cm-flow-grey-2: #4C4B4A;
    --cm-flow-grey-3: #7F7F7D;
    --cm-flow-grey-4: #B2B1AD;
    --cm-flow-grey-5: #D8D6D2;
    --cm-flow-grey-6: #FBF8F1;
    --cm-flow-white: #FFFFFF;
    --cm-flow-red: #FF7477;
    --cm-flow-red-light: #EFCECE;
    --cm-flow-orange: #FEA953;
    --cm-flow-orange-light: #FEE2C2;
    --cm-flow-yellow: #FFE918;
    --cm-flow-yellow-light: #F4F2BD;
    --cm-flow-green: #87E878;
    --cm-flow-green-light: #D5F3E1;
    --cm-flow-cyan: #6DE2DE;
    --cm-flow-cyan-light: #DAF7F7;
    --cm-flow-blue: #3E85CC;
    --cm-flow-blue-light: #C6CBE5;
    --cm-flow-purple: #775CE0;
    --cm-flow-purple-light: #D7BBEB;
    --cm-flow-pink: #E55ECC;
    --cm-flow-pink-light: #FACAE9;

    // :::::: theme colors ::::::
    --cm-flow-foreground-color: 
      var(--sn-stylekit-foreground-color, var(--cm-flow-black));
    --cm-flow-background-color: 
      var(--sn-stylekit-background-color, var(--cm-flow-white));

    --cm-flow-selection-background: 
      var(--sn-selection-background, #3E85CC30);
    --cm-flow-activeLine-background: 
      var(--sn-stylekit-grey-4-opacity-variant, #D8D6D230);
    --cm-flow-cursor-color: 
      var(--sn-stylekit-info-color, var(--cm-flow-blue));

    --cm-flow-content-separator-color: 
      var(--sn-stylekit-foreground-color, var(--cm-flow-black));
  }

  .cm-editor {
    font-size: var(--cm-flow-global-font-size);
    letter-spacing: var(--cm-flow-global-letter-spacing);
    font-kerning: normal;
    font-style: normal;
    color: var(--cm-flow-foreground-color);
    background-color: var(--cm-flow-background-color);

    &.cm-focused {
      outline: 0 !important;
    }

    .cm-gutters {
      background: none;
      border-right: 0;
      color: var(--cm-flow-foreground-color);

      & .cm-activeLineGutter {
        ${activeLineBackground}
      }

      .cm-lineNumbers {
        .cm-gutterElement {
          font-family: var(--cm-flow-monospace-font);
          font-size: 0.8em;
          text-align: right;
          line-height: 2.2;
          opacity: 0.2;
          display: inline-block;
          min-width: 2.5em;

          &.cm-activeLineGutter {
            opacity: 1;
          }
        }
      }
    }

    .cm-content {
      .cm-line {
        position: relative;
        padding: var(--cm-flow-global-line-spacing) 4px;
        font-family: var(--cm-flow-sans-serif-font, sans-serif);

        &.cm-activeLine {
          ${activeLineBackground}
        }
      }

      .cm-matchingBracket {

      }

      .cm-specialChar {
        color: var(--cm-flow-foreground-color);
        opacity: 0.1;
      }
    }

    .cm-cursorLayer {
      animation-name: ${cursorBlink} !important;

      .cm-cursor {
        border-width: 2px;
        border-color: var(--cm-flow-cursor-color);
      }
    }

    .cm-selectionLayer {
      .cm-selectionBackground {
        background-color: var(--cm-flow-selection-background);
      }
    }

  }

  // move the search panel to the bottom of the space
  .cm-panels {
    position: fixed;
  }

  .flow {

    // hide the processingInstruction tags on non-active lines 
    &-processingInstruction {
      display: inline-block;
      width: 0;
      visibility: hidden;

      .cm-activeLine & {
        width: auto;
        visibility: visible;
      }
    }

    &-heading {
      font-weight: bold;

      &-1 {
        font-weight: bold;
        font-size: 1.5em;
        padding: 0.3em 0 0.6em 0;
      }

      &-2 {
        font-weight: bold;
        font-size: 1.2em;
        padding: 0.2em 0 0.4em 0;
      }

      &-3 {
        font-weight: bold;
        font-size: 1em;
        padding: 0.1em 0 0.2em 0;
      }

      &-4 {
        font-weight: normal;
        font-style: italic;
        font-size: 1em;
        padding: 0.1em 0 0.2em 0;
      }

      &-5 {
        font-weight: normal;
        text-decoration: underline;
        font-size: 1em;
        padding: 0.1em 0 0.2em 0;
      }

      &-6 {
        font-weight: normal;
        font-size: 1em;
        padding: 0.1em 0 0.1em 0;
      }
    }

    &-monospace {
      font-family: var(--cm-flow-monospace-font);
      font-size: 0.9em;

      &.flow-fencedCode {
        &.flow-labelName {
          padding-left: 1ch;
          padding-right: 1ch;
          color: var(--cm-flow-background-color);
          background: var(--cm-flow-foreground-color);
          border-radius: 4px;
        }

        &.flow-processingInstruction {
          &:after {
            content: '';
            position: absolute;
            width: 100%;
            height: 1px;
            top: 50%;
            left: 0;
            border-top: 1px solid var(--cm-flow-content-separator-color);
            z-index: -1;
            visibility: visible;
            opacity: 0.2;
          }

          .cm-activeLine & {
            &:after {
              visibility: hidden;
            }
          }
        }
      }
    }

    &-contentSeparator {
      visibility: hidden;

      &:after {
        content: '';
        position: absolute;
        width: 100%;
        height: 1px;
        top: 50%;
        left: 0;
        border-top: 2px solid var(--cm-flow-content-separator-color);
        z-index: -1;
        visibility: visible;
      }

      .cm-activeLine & {
        visibility: visible; 
        
        &:after {
          visibility: hidden;
        }
      }
    }

    &-list {
      // bullets or numbering 
      &.flow-processingInstruction:first-child {
        width: auto;
        visibility: visible;
        font-weight: bold;
        padding-left: 2ch;
      }

      // checkboxes
      &.flow-atom {
        font-weight: bold;
      }
    }
  }
`;

export {
  EditorCMStyles
}