/**
 * Highlight style tags to match flow markdown language parser
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  flowHighlightStyle
 */
import { tags, HighlightStyle } from '@codemirror/highlight'

/**
 * Highlight style that applies 'flow-' prefixed classes to style tags.
 * @uses  HighlightStyle
 */
const flowHighlightStyle = HighlightStyle.define([
  // standard tags 
  // @see  https://github.com/codemirror/highlight/blob/main/src/highlight.ts#L848
  {tag: tags.link, class: 'flow-link'},
  {tag: tags.heading, class: 'flow-heading'},
  {tag: tags.emphasis, class: 'flow-emphasis'},
  {tag: tags.strong, class: 'flow-strong'},
  {tag: tags.keyword, class: 'flow-keyword'},
  {tag: tags.atom, class: 'flow-atom'},
  {tag: tags.bool, class: 'flow-bool'},
  {tag: tags.url, class: 'flow-url'},
  {tag: tags.labelName, class: 'flow-labelName'},
  {tag: tags.inserted, class: 'flow-inserted'},
  {tag: tags.deleted, class: 'flow-deleted'},
  {tag: tags.literal, class: 'flow-literal'},
  {tag: tags.string, class: 'flow-string'},
  {tag: tags.number, class: 'flow-number'},
  {tag: [tags.regexp, tags.escape, tags.special(tags.string)], class: 'flow-string2'},
  {tag: tags.variableName, class: 'flow-variableName'},
  {tag: tags.local(tags.variableName), class: 'flow-variableName flow-local'},
  {tag: tags.definition(tags.variableName), class: 'flow-variableName flow-definition'},
  {tag: tags.special(tags.variableName), class: 'flow-variableName2'},
  {tag: tags.definition(tags.propertyName), class: 'flow-propertyName flow-definition'},
  {tag: tags.typeName, class: 'flow-typeName'},
  {tag: tags.namespace, class: 'flow-namespace'},
  {tag: tags.className, class: 'flow-className'},
  {tag: tags.macroName, class: 'flow-macroName'},
  {tag: tags.propertyName, class: 'flow-propertyName'},
  {tag: tags.operator, class: 'flow-operator'},
  {tag: tags.comment, class: 'flow-comment'},
  {tag: tags.meta, class: 'flow-meta'},
  {tag: tags.invalid, class: 'flow-invalid'},
  {tag: tags.punctuation, class: 'flow-punctuation'},

  // additional @codemirror/lang-markdown tags
  // @see  https://github.com/codemirror/lang-markdown/blob/main/src/markdown.ts#L10
  {tag: tags.heading1, class: 'flow-heading-1'},
  {tag: tags.heading2, class: 'flow-heading-2'},
  {tag: tags.heading3, class: 'flow-heading-3'},
  {tag: tags.heading4, class: 'flow-heading-4'},
  {tag: tags.heading5, class: 'flow-heading-5'},
  {tag: tags.heading6, class: 'flow-heading-6'},
  {tag: tags.quote, class: 'flow-quote'},
  {tag: tags.contentSeparator, class: 'flow-contentSeparator'},
  {tag: tags.character, class: 'flow-character'},
  {tag: tags.list, class: 'flow-list'},
  {tag: tags.quote, class: 'flow-quote'},
  {tag: tags.monospace, class: 'flow-monospace'},
  {tag: tags.content, class: 'flow-content'},
  {tag: tags.processingInstruction, class: 'flow-processingInstruction'},
  {tag: tags.special(tags.monospace), class: 'flow-monospace flow-fencedCode'}
])

export {
  flowHighlightStyle
}