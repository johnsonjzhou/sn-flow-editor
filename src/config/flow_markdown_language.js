/**
 * Flow markdown language configuration, adapted with customisations 
 * from @codemirror\lang-markdown
 * @see  https://github.com/codemirror/lang-markdown/blob/main/src/markdown.ts
 * 
 * Varied features:
 * - Using extended feature set including GFM
 * - FencedCode tag 
 * - WhitespaceChar tag 
 * 
 * @export  flowMarkdownLanguage
 */
import { Language, defineLanguageFacet, languageDataProp, foldNodeProp, indentNodeProp } 
  from '@codemirror/language'
import { styleTags, tags as t } from '@codemirror/highlight'
import { parser as baseParser, GFM, Subscript, Superscript, Emoji } 
  from '@lezer/markdown'

const data = defineLanguageFacet({block: {open: '<!--', close: '-->'}})

const flowMarkdown = baseParser.configure([GFM, Subscript, Superscript, Emoji, {
  props: [
    styleTags({
      // commonmark 
      'Blockquote/...': t.quote,
      HorizontalRule: t.contentSeparator,
      'ATXHeading1/... SetextHeading1/...': t.heading1,
      'ATXHeading2/... SetextHeading2/...': t.heading2,
      'ATXHeading3/...': t.heading3,
      'ATXHeading4/...': t.heading4,
      'ATXHeading5/...': t.heading5,
      'ATXHeading6/...': t.heading6,
      'Comment CommentBlock': t.comment,
      Escape: t.escape,
      Entity: t.character,
      'Emphasis/...': t.emphasis,
      'StrongEmphasis/...': t.strong,
      'Link/... Image/...': t.link,
      'OrderedList/... BulletList/...': t.list,
      'BlockQuote/...': t.quote,
      'InlineCode CodeText': t.monospace,
      URL: t.url,
      'HeaderMark HardBreak QuoteMark ListMark LinkMark EmphasisMark CodeMark': t.processingInstruction,
      'CodeInfo LinkLabel': t.labelName,
      LinkTitle: t.string,
      Paragraph: t.content, 

      // extended 
      'TableDelimiter SubscriptMark SuperscriptMark StrikethroughMark': t.processingInstruction,
      'TableHeader/...': t.heading,
      'Strikethrough/...': t.strikethrough,
      TaskMarker: t.atom,
      Task: t.list,
      Emoji: t.character,
      'Subscript Superscript': t.special(t.content),
      TableCell: t.content, 

      // varied 
      'FencedCode/...': t.special(t.monospace) 
    }),
    foldNodeProp.add(type => {
      if (!type.is('Block') || type.is('Document')) return undefined
      return (tree, state) => ({from: state.doc.lineAt(tree.from).to, to: tree.to})
    }),
    indentNodeProp.add({
      Document: () => null
    }),
    languageDataProp.add({
      Document: data
    })
  ]
}]);

const mkLang = (parser) => {
  return new Language(data, parser, parser.nodeSet.types.find(t => t.name == 'Document'))
}

const flowMarkdownLanguage = mkLang(flowMarkdown);

export {
  flowMarkdownLanguage
}