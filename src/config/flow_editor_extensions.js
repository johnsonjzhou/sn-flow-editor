/**
 * Editor extension config for CodeMirror 6
 * @author  Johnson Zhou <johnson@simplyuseful.io>
 * 
 * @export  flowEditorExtensions
 */

import { keymap, highlightSpecialChars, drawSelection, highlightActiveLine } 
  from '@codemirror/view'
import { history, historyKeymap } from '@codemirror/history'
import {foldGutter } from '@codemirror/fold'
import {lineNumbers, highlightActiveLineGutter} from '@codemirror/gutter'
import { indentMore, indentLess, insertNewlineAndIndent, deleteLine } 
  from '@codemirror/commands'
import { bracketMatching } from '@codemirror/matchbrackets'
import { closeBrackets } from '@codemirror/closebrackets'
import {searchKeymap, highlightSelectionMatches} from '@codemirror/search'

import { markdown } from '@codemirror/lang-markdown';
import { languages } from '@codemirror/language-data';

import { flowMarkdownLanguage } from './flow_markdown_language'
import { flowHighlightStyle } from './flow_highlight_style'

import { onChangeCallback } from '../modules/cm_update';

const flowEditorExtensions = [

  // add line numbers to gutter 
  lineNumbers(), 

  // highlight the active line in the gutter
  highlightActiveLineGutter(), 

  // highlight the active line 
  highlightActiveLine(), 

  // highlight whitespace 
  highlightSpecialChars({ specialChars: /\s/g }), 

  // cusor and selection 
  drawSelection(), 
  highlightSelectionMatches(),

  // highlight matching brackets 
  bracketMatching(),

  // auto close brackets
  closeBrackets(), 

  // fold the gutter for chunks of content
  foldGutter(), 

  // undo redo function
  history(), 

  // parser 
  // codeLanguages: languages allows for embedded code blocks
  markdown({ base: flowMarkdownLanguage, codeLanguages: languages }), 

  // syntax highlighting
  flowHighlightStyle,

  // watch for doc changes
  onChangeCallback((viewUpdate) => {
    //todo  to attach SN content streams
    //console.log(viewUpdate && viewUpdate.state.doc.text.join('\r\n'));
  }),

  // keymaps
  keymap.of([

    // search function
    ...searchKeymap,

    // undo/redo function
    ...historyKeymap, 

    // custom keymaps
    { key: 'Shift-Tab', run: indentLess },
    { key: 'Tab', run: indentMore},
    { key: 'Enter', run: insertNewlineAndIndent },
    { key: 'Ctrl-Shift-k', mac: 'Cmd-Shift-k', run: deleteLine }
  ])
];

export {
  flowEditorExtensions
}