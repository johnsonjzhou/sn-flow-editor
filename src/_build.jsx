import React from 'react';
import ReactDOM from 'react-dom';

import Editor from './editor';
import { ErrorBoundary } from 'flow-utils';

/**
 * Show identifier in the console
 */
 window.console.log(
  '--------------------------------------------------- \n'+
  'Markdown Flow Editor for Standard Notes \n'+
  `Version ${___WEBPACK_VERSION___} ${process.env.NODE_ENV} \n`+
  'Repository (https://bitbucket.org/johnsonjzhou/sn-flow-editor/) \n'+
  '---------------------------------------------------'
);

/**
 * Initiate Editor on <main>
 */
 ReactDOM.render(
  <ErrorBoundary message='Editor failed to load'>
    <Editor/>
  </ErrorBoundary>
, document.getElementsByTagName("main")[0]);